import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { AngularFire } from 'angularfire2';
import firebase from 'firebase';

/*
  Generated class for the AuthService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class AuthService {

  public fireAuth: any;
 

  constructor(public http: Http, public af: AngularFire) {
    af.auth.subscribe( user => {
      if (user) { this.fireAuth = user.auth; }
    });
  }

  // login accept email and password as string
  doLogin(email: string, password: string): any {

    return this.af.auth.login({
      email: email,
      password: password
    });
  }

  register(email: string, password: string): any {
     return this.af.auth.createUser({ 
      email: email, 
      password: password 
    });
  }

  resetPassword(email: string): any {
    return firebase.auth().sendPasswordResetEmail(email);
  }

  doLogout(): any {
    return this.af.auth.logout();
  }

}
